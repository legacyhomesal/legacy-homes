Legacy Homes is your premier new home builder in Huntsville and north Alabama. Our communities feature our award-winning designs with stunning amenities, excellent schools, and convenient access to Redstone Arsenal, Bridge Street, and more.

Address: 2000 Andrew Jackson Way NE, Huntsville, AL 35811, USA

Phone: 256-361-9104

Website: http://www.legacyhomesal.com
